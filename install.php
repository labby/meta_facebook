<?php

/**
 * @module          Meta Facebook
 * @author          cms-lab
 * @copyright       2017-2023 cms-lab
 * @link            https://cms-lab.com
 * @license         custom license: https://cms-lab.com/_documentation/meta-facebook/license.php
 * @license_terms   please see license
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

// mod_meta_facebook
$table_fields="
	  `id` INT NOT NULL AUTO_INCREMENT,
	  `author` varchar(32) NOT NULL DEFAULT 'author',
	  `default_image` varchar(256) NOT NULL DEFAULT 'http://cms-lab.com/_documentation/media/meta_facebook/fb.jpg',
	  `site_name` varchar(64) NOT NULL DEFAULT 'name',
	  `content_type` varchar(128) NOT NULL DEFAULT 'website',
	  PRIMARY KEY ( `id` )
	";
LEPTON_handle::install_table("mod_meta_facebook_settings", $table_fields);

// insert some default values
$field_values="
(NULL, 'author', 'http://cms-lab.com/_documentation/media/meta_facebook/fb.jpg', 'name', 'website')
";
LEPTON_handle::insert_values("mod_meta_facebook_settings", $field_values);

// import default droplets
LEPTON_handle::install_droplets('meta_facebook', 'droplet_meta-facebook');
