<?php

/**
 * @module          Meta Facebook
 * @author          cms-lab
 * @copyright       2017-2023 cms-lab
 * @link            https://cms-lab.com
 * @license         custom license: https://cms-lab.com/_documentation/meta-facebook/license.php
 * @license_terms   please see license
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

$module_directory     = "meta_facebook";
$module_name          = "Meta Facebook";
$module_function      = "tool";
$module_version       = "1.3.0";
$module_platform      = "7.x";
$module_author        = '<a href="https://cms-lab.com" target="_blank">CMS-LAB</a>';
$module_license       = '<a href="https://cms-lab.com/_documentation/meta-facebook/license.php" class="info" target="_blank">Custom license</a>';
$module_license_terms = "please see license";
$module_description   = "Tool to edit and insert special facebook meta-tags";
$module_guid		  = "8325d253-edf0-47fd-be95-193180776a0a";
