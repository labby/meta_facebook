/**
 * @module          Meta Facebook
 * @author          cms-lab
 * @copyright       2017-2023 cms-lab
 * @link            https://cms-lab.com
 * @license         custom license: https://cms-lab.com/_documentation/meta-facebook/license.php
 * @license_terms   please see license
 *
 */


$('.ui.dropdown')
  .dropdown()
;

$('.ui.checkbox')
  .checkbox()
;

